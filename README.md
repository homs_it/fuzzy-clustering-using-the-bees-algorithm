# BeesAlgorithm

 The Bees are represented in an array called (bees_array), 
 each row represent one bee and each column represent the value of each dimension the last column of the array 
 saves the values of the fitness function and other columns represent a dimension of the bee

The algorithm needs different parameters (found in parameters.m file) as follows:

n          % number of bees
m          % number of nighbour sites 
e          % number of elete bees (Best of Best)
n1         % number of bees sent to m (nighbourhood search)
n2         % number of bees sent around e 
iter       % number of iteration (How many times the generation will be created)

